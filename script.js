

var gCoursesDB = {
    description: "This DB includes all courses in system",
    courses: [
        {
            id: 1,
            courseCode: "FE_WEB_ANGULAR_101",
            courseName: "How to easily create a website with Angular",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-angular.jpg",
            teacherName: "Morris Mccoy",
            teacherPhoto: "images/teacher/morris_mccoy.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 2,
            courseCode: "BE_WEB_PYTHON_301",
            courseName: "The Python Course: build web application",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-python.jpg",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 5,
            courseCode: "FE_WEB_GRAPHQL_104",
            courseName: "GraphQL: introduction to graphQL for beginners",
            price: 850,
            discountPrice: 650,
            duration: "2h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-graphql.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 6,
            courseCode: "FE_WEB_JS_210",
            courseName: "Getting Started with JavaScript",
            price: 550,
            discountPrice: 300,
            duration: "3h 34m",
            level: "Beginner",
            coverImage: "images/courses/course-javascript.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 8,
            courseCode: "FE_WEB_CSS_111",
            courseName: "CSS: ultimate CSS course from beginner to advanced",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-css.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 14,
            courseCode: "FE_WEB_WORDPRESS_111",
            courseName: "Complete Wordpress themes & plugins",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-wordpress.jpg",
            teacherName: "Clevaio Simon",
            teacherPhoto: "images/teacher/clevaio_simon.jpg",
            isPopular: true,
            isTrending: false
        }
    ]
}
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
$(document).ready(function () {
    loadCourseMostPopular();
    loadCourseTrending();
})
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function loadCourseMostPopular() {
    "use strict";
    var vCourseMostPopular = [];
    for (var bI = 0; bI < gCoursesDB.courses.length; bI++) {
        if (gCoursesDB.courses[bI].isPopular) {
            vCourseMostPopular.push(gCoursesDB.courses[bI])
        }
    }
    console.log(vCourseMostPopular);
    showCourseMostPopular(vCourseMostPopular);
}

function loadCourseTrending() {
    "use strict";
    var vCourseTrending = [];
    for (var bI = 0; bI < gCoursesDB.courses.length; bI++) {
        if (gCoursesDB.courses[bI].isTrending) {
            vCourseTrending.push(gCoursesDB.courses[bI])
        }
    }
    console.log(vCourseTrending);
    showCourseTrending(vCourseTrending);
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function showCourseMostPopular(paramCourseMostPolpular) {
    "use strict";
    var vAddMostPopular = "";
    for (var bI = 0; bI < paramCourseMostPolpular.length; bI++) {
        vAddMostPopular += `   <div class="col-sm-3">
                                    <div class="container">
                                        <div class="card" style="width:16rem;">
                                            <img class="card-img-top" src="` + paramCourseMostPolpular[bI].coverImage + `"alt="">
                                            <div class="card-body">
                                                <p class="card-title text-primary font-weight-bold">` + paramCourseMostPolpular[bI].courseName + `</p>
                                                <p class="card-text"><i class="far fa-clock"></i>&nbsp;` + paramCourseMostPolpular[bI].duration + `&nbsp;&nbsp;` + paramCourseMostPolpular[bI].level + `</p>
                                                <p class="card-text font-weight-bold" style="float:left;">$` + paramCourseMostPolpular[bI].discountPrice + `&nbsp;</p>
                                                <p class="text-black-50"><s>$` + paramCourseMostPolpular[bI].price + `</s></p>
                                            </div>
                                            <div class="card-footer">
                                                <div class="row">
                                                    <div class="col-sm-10">
                                                        <img src="` + paramCourseMostPolpular[bI].teacherPhoto + `" class="rounded-circle" alt="juanita_bell"
                                                        width="40px">&nbsp;
                                                        ` + paramCourseMostPolpular[bI].teacherName + `
                                                    </div>
                                                    <div class="col-sm-2 d-flex align-items-center">
                                                        <i class="far fa-bookmark"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>`
    }
    $('#course-most-popular').html(vAddMostPopular);
}

function showCourseTrending(paramCourseTrending) {
    "use strict";
    var vAddTrending = "";
    for (var bI = 0; bI < paramCourseTrending.length; bI++) {
        vAddTrending += `   <div class="col-sm-3">
                                <div class="container">
                                    <div class="card" style="width:16rem;">
                                        <img class="card-img-top" src="` + paramCourseTrending[bI].coverImage + `"alt="">
                                        <div class="card-body">
                                            <p class="card-title text-primary font-weight-bold">` + paramCourseTrending[bI].courseName + `</p>
                                            <p class="card-text"><i class="far fa-clock"></i>&nbsp;` + paramCourseTrending[bI].duration + `&nbsp;&nbsp;` + paramCourseTrending[bI].level + `</p>
                                            <p class="card-text font-weight-bold" style="float:left;">$` + paramCourseTrending[bI].discountPrice + `&nbsp;</p>
                                            <p class="text-black-50"><s>$` + paramCourseTrending[bI].price + `</s></p>
                                        </div>
                                        <div class="card-footer">
                                            <div class="row">
                                                <div class="col-sm-10">
                                                    <img src="` + paramCourseTrending[bI].teacherPhoto + `" class="rounded-circle" alt="juanita_bell"
                                                    width="40px">&nbsp;
                                                    ` + paramCourseTrending[bI].teacherName + `
                                                </div>
                                                <div class="col-sm-2 d-flex align-items-center">
                                                    <i class="far fa-bookmark"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>`
    }

    $('#course-trending').html(vAddTrending)
}